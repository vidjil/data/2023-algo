import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import pandas
import numpy as np
from math import sqrt, isnan
import os
import re
import sys
from functools import cmp_to_key
from create_plot import BAR_WIDTH, sort_labels

def int_nan(value):
    '''
    Convert to int and returns an empty string if the value is nan
    '''
    return int(value) if not isnan(value) else ''

if __name__ == '__main__':

    if len(sys.argv) < 4:
        print("Usage: {} <.detect.tsv file> <.assign.tsv file> <.should-vdj.fa>".format(sys.argv[0]))
        sys.exit(1)

    infile = sys.argv[1]
    if infile.endswith('.detect.tsv'):
        basename = infile[:-len('.detect.tsv')]
    else:
        basename = os.path.splitext(infile)[0]
    outpdf = basename+".pdf"
    outtex = basename+".tex"

    assign = pandas.read_csv(sys.argv[2], delimiter='\t').drop(columns='Nb sequences')
    detect = pandas.read_csv(sys.argv[1], delimiter='\t').drop(columns='Nb sequences')

    softs = sorted(list(detect.columns[1:]), key=cmp_to_key(sort_labels), reverse=True)
    all_locus = [ x for x in detect['locus'] if isinstance(x, str) ]
    nrows = int(sqrt(len(all_locus)))
    ncols = len(all_locus) // nrows + (1 if len(all_locus) % nrows > 0 else 0)

    results = {}
    nb = {}
    for soft in softs:
        results[soft] = {}
        for locus in all_locus:
            results[soft][locus] = {'detect': detect.loc[detect['locus']==locus][soft].iloc[0],
                                    'assign': assign.loc[assign['locus']==locus][soft].iloc[0]}
    for locus in all_locus:
        nb[locus] = sum([ re.match(r'^>.*\['+locus.replace('+', '\+')+'\].*$', l.rstrip()) is not None for l in open(sys.argv[3]).readlines()])

    fig, axs = plt.subplots(nrows=nrows, ncols=ncols, figsize=(ncols*3, nrows*1.8), constrained_layout=True, sharey=True)
    axs = axs.flatten()  # flatten the 2D array of subplots for easier indexing
    for i, locus in enumerate(all_locus):
        ax = axs[i]  # get the axis object for the current subplot
        soft_detect = [ results[x][locus]['detect'] for x in softs ]
        soft_assign = [ results[x][locus]['assign'] for x in softs ]
        evals = nb[locus]  # get the number of evaluations for the current data
        # Define x-positions and bar widths
        x = np.arange(len(softs))

        # Create the bars for each software
        ax.barh(x+.6*BAR_WIDTH, soft_detect, BAR_WIDTH, label='detect')
        ax.barh(x-.6*BAR_WIDTH, soft_assign, BAR_WIDTH, label='assign')

        ax.set_yticklabels(softs)
        ax.set_yticks(range(len(softs)))

        # Set axis limits and labels
        ax.set_xlim([0, evals])
        ax.xaxis.set_major_locator(MaxNLocator(integer=True, prune='both', nbins=5))
        xticks = list(ax.get_xticks())
        if evals not in xticks:
            xticks.append(evals)
            ax.set_xticks(xticks)
        ax.set_title(locus)

        # Add horizontal grid lines and legend
        ax.grid(axis='x', linestyle='--')

    # xlabel on the last row only
    for i in range(1, ncols+1):
        axs[-i].set_xlabel('Number of sequences')

    if nrows > 1:
        fig.legend(['Detection', 'Designation'], loc='lower center', bbox_to_anchor=(0.5, min(0,-.05+.02*(nrows-1))), ncol=2)
        fig.tight_layout(rect=[0, 0.03, 1, 1])
    else:
        fig.legend(['Detection', 'Designation'], loc='center left', bbox_to_anchor=(.75, .7), ncol=1)
        fig.tight_layout(rect=[0, 0.03, .8, 1])
        
    # Show the plot
    plt.savefig(outpdf)

    tabular = f'\\begin{{tabular}}{{l{"|cc"*len(all_locus)}}}\n'
    for locus in all_locus:
        tabular += f"& \multicolumn{{2}}{{c}}{{{locus}}}"
    tabular += "\\\\\nsequences"
    for locus in all_locus:
        tabular += f"& \multicolumn{{2}}{{c}}{{{nb[locus]}}}"
    tabular += "\\\\\n"
    for locus in all_locus:
        tabular += "& detect. & design."
    tabular += "\\\\\n"

    for soft in sorted(softs, key=cmp_to_key(sort_labels)):
        tabular += soft
        for locus in all_locus:
            tabular += f"& {int_nan(results[soft][locus]['detect'])} & {int_nan(results[soft][locus]['assign'])}"
        tabular += "\\\\\n"
    tabular += "\end{tabular}\n"

    f = open(str(outtex), "w")
    print(tabular, file=f)
    f.close()
    

