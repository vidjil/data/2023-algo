# Benchmark from *Alignment-free detection and seed-based identification of multi-loci V(D)J recombinations in Vidjil-algo*

The Snakefile in this repository will reproduce the results obtained in the
aforementioned paper by Cyprien Borée, Mathieu Giraud and Mikaël Salson on a
benchmark of two versions of MiXCR and two versions of Vidjil-algo on several
real and simulated datasets.

## Requirements

- Snakemake ≥ 6.15
- pandas


The config.json file needs to be completed with the paths to where should be
stored the software (`softs`), less than 1GB, the data for the benchmarks
(`benchs`), less than 5GB, the log files (`logs`), less than 10MB, and the results (`results`), around 20-30GB.

In order to run the benchmarks on MiXCR, you need to get [a license from
MiXCR](https://licensing.milaboratories.com/). Then the
string corresponding to the license must be provided in the `mixcr_license` key.
If the license key is left empty, only Vidjil-algo will be launched.

## Running

Once the `config.json` file has been filled, you only need to launch `snakemake` with the `--use-conda` parameter and the desired number of threads.

Example:
```
sakemake -j 5 --use-conda
```

All the final results, including the plots from the article, will be produced
under the `final` subdirectory in the `results`directory that was specified in
the `config.json` file.
