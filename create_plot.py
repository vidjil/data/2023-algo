#!/usr/bin/python3
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from collections import defaultdict
from functools import cmp_to_key
import csv
import os
import sys
import re

from math import ceil, floor, log10

BAR_WIDTH=.3
AFZAL_ERRORS= { '0': 'No error',
                '0.001-0.0002': 'Low error',
                '0.01-0.0002': 'Medium error'}


def sort_labels(a, b):
    if a == 'vidjil-new':
        return 1
    elif b == 'vidjil-new':
        return -1
    else:
        return 1 if a > b else -1

def sort_first_labels(a, b):
    return sort_labels(a[0], b[0])

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("Usage: {} <.csv file>".format(sys.argv[0]))
        sys.exit(1)

    infile = sys.argv[1]
    basename = os.path.splitext(infile)[0]
    outfile = basename+".pdf"

    results = defaultdict(list)

    with open(infile, "r") as f:
        reader = csv.reader(f, delimiter='\t')
        for row in reader:
            id = row[0]
            if 'Nb seq' in id:
                next
            if 'afzal' in id:
                params = re.search(r'afzal-file_([0-9.-]+)error_(.)sample', id)
                if params and len(params.groups()) >= 2 and params.groups()[0] in AFZAL_ERRORS:
                    id = f'Afzal - {params.groups()[1]} - {AFZAL_ERRORS[params.groups()[0]]}'
            results[id].append(row[1:])

    for k in results.keys():
        results[k] = sorted(results[k], key=cmp_to_key(sort_first_labels), reverse=True)
        
    f, axarr = plt.subplots(len(results), 3, sharey='all', figsize=(10, (1+1/(1.5*len(results)))*len(results)))
    plot_nb=0
    for key in sorted(results.keys()):
        times=[float(i[1]) for i in results[key]]
        # log_scale=(max(times)/min(times) > 10)
        log_scale = True
        if len(results) == 1:
            current_ax = axarr
        else:
            current_ax = axarr[plot_nb]

        current_ax[0].barh(range(len(results[key])), times, BAR_WIDTH, log=log_scale)
        # current_ax[0].set_title('time '+ ('(min)' if log_scale else '(sec)'))
        current_ax[0].set_yticklabels([i[0] for i in results[key]])
        current_ax[0].set_yticks(range(len(results[key])))
        current_ax[0].set_xlim(10**floor(log10(min(times))), 10**ceil(log10(max(times))))
        current_ax[0].xaxis.set_major_locator(ticker.LogLocator(numticks=999))
        current_ax[0].xaxis.set_minor_locator(ticker.LogLocator(numticks=999, subs="auto"))
        # if log_scale:
        #     current_ax[0].xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x//6)/10)))
        current_ax[1].barh(range(len(results[key])), [round(float(i[2])) for i in results[key]], BAR_WIDTH)
        # current_ax[1].set_title('memory (MB)')
        current_ax[1].xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x/1e3)))
        values=[round(float(i[3]))*100./round(float(i[4])) for i in results[key]]
        current_ax[2].barh(range(len(results[key])), values, BAR_WIDTH)
        # current_ax[2].set_title(u'({} sequences)'.format(results[key][0][4]),
        #                             fontsize=8)
        current_ax[2].xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: '{0:.2f}'.format(x)))
        maxval = max(values)
        minval = min(values)
        current_ax[2].set_xlim((max(0,min(minval - (maxval-minval)/5,99.9)) , maxval if minval < 50 else 100))

        plot_nb += 1

    for ax, row in zip(axarr[:,0] if len(results) > 1 else [axarr[0]], sorted(results.keys())):
        ax.set_ylabel(row+'\n{:,} seq.'.format(round(float(results[row][0][4]))), rotation=0, size='large')
        ax.yaxis.set_label_coords(-1, 0.25)
    for ax, col in zip(axarr[0] if len(results) > 1 else axarr, ['time (s)', 'memory (MB)', '% detection']):
        ax.set_xlabel(col, rotation=0, size='large')
        ax.xaxis.set_label_coords(.5, 1.4)
    if len(results) > 1:
        plt.tight_layout()
    else:
        plt.gcf().subplots_adjust(bottom=0.2, top=.6, right=.99, left=.3)
    plt.savefig(outfile)
