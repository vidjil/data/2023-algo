import os
import glob

# prerequisites
# make, git, g++
# jq

#igrec
# cmake

configfile: "config.json"

SOFTS_DIR = os.path.realpath(config["softs"])
VIDJIL_DIR = os.path.realpath(SOFTS_DIR + "/vidjil-dev")
BENCHS_DIR = os.path.realpath(config["benchs"])
RESULTS_DIR = os.path.realpath(config["results"])
LOGS_DIR = os.path.realpath(config["logs"])
MIXCR_LICENSE= config['mixcr_license']

os.makedirs(LOGS_DIR, exist_ok = True)

AFZAL_DATASET=['file_0.001-0.0002error_Asample.1.fastq.gz',
               'file_0.001-0.0002error_Bsample.1.fastq.gz',
               'file_0.001-0.0002error_Csample.1.fastq.gz',
               'file_0.001-0.0002error_Dsample.1.fastq.gz',
               'file_0.01-0.0002error_Asample.1.fastq.gz',
               'file_0.01-0.0002error_Bsample.1.fastq.gz',
               'file_0.01-0.0002error_Csample.1.fastq.gz',
               'file_0.01-0.0002error_Dsample.1.fastq.gz',
               'file_0error_Asample.1.fastq.gz',
               'file_0error_Bsample.1.fastq.gz',
               'file_0error_Csample.1.fastq.gz',
               'file_0error_Dsample.1.fastq.gz']
COMPLETE_LOCUS=['IGH', 'IGK', 'IGL', 'TRA', 'TRB', 'TRD', 'TRG']
INCOMPLETE_LOCUS=['IGH+', 'IGK+', 'TRA+D', 'TRB+', 'TRD+']
LOCUS = COMPLETE_LOCUS + INCOMPLETE_LOCUS
VIDJIL_OLD = '2018.02'
VIDJIL_NEW = '2025.02'
SOFTS = ['vidjil-'+VIDJIL_NEW, 'vidjil-'+VIDJIL_OLD]
if len(MIXCR_LICENSE) > 0:
    SOFTS += ['mixcr3', 'mixcr4'] #, 'igrec']
MIXCR_LIB_BASENAME="vidjilbench"
MIXCR_IMGT_LIBRARY=os.path.expanduser("~")+"/.mixcr/libraries/"+MIXCR_LIB_BASENAME+".json.gz"
MIXCR3_LIBRARY_filename = 'imgt.202214-2.sv8.json.gz'
MIXCR3_LIBRARY_url = 'https://github.com/repseqio/library-imgt/releases/download/v8/'+MIXCR3_LIBRARY_filename
MIXCR3_LIBRARY_PATH=SOFTS_DIR+"/"+MIXCR3_LIBRARY_filename
ERRORS=[".02-0", ".05-0", ".1-0", ".05-.2"]


rule all:
    input:
        expand(RESULTS_DIR+"/final/bench_{file}_shouldvdj.{format}",
               file=["vidjil-designated", "ligm-db-homo-sapiens"],
               format=['pdf', 'tex']),
        expand(RESULTS_DIR+"/final/{file}.pdf", file=["random.fa"]),
        expand(RESULTS_DIR+"/final/afzal-{file}.pdf", file=AFZAL_DATASET),
        RESULTS_DIR+"/final/afzal_summary.pdf",
        expand(RESULTS_DIR+"/final/random_vdj-{error}{status}.pdf",
               error=ERRORS, status=['', '_complete', '_incomplete'])


#
# INSTALLING SOFTWARE
#
def install_vidjil(output, branch, demo=False, compile=False, germline=True):
    dir=os.path.dirname(str(output))
    cmd = ["cd {dir}".format(dir=dir)]

    if not os.path.exists(dir) or not os.path.exists(dir+"/.git"):
        cmd.append("git init")
        cmd.append("git remote add origin https://gitlab.inria.fr/vidjil/vidjil.git")
    else:
        cmd.append("git checkout -- . && git clean -f");
    cmd.append("git pull origin {branch}".format(branch=branch))
    if germline:
        cmd.append("make germline")
    if demo:
        cmd.append("make demo")
    if compile:
        cmd.append('make CXXFLAGS="-W -Wall -std=c++11 -O2 -DNDEBUG -I \\"$CONDA_PREFIX\\"/include" CFLAGS="-I \\"$CONDA_PREFIX\\"/include"')
    logfile = LOGS_DIR+"/vidjil-install-"+branch.replace('/','_')+".log"
    cmdline = '(' + ' && '.join(cmd) + ') > ' + logfile + ' 2> '+logfile
    return (cmdline)

rule install_vidjil:
    output:
        SOFTS_DIR+"/vidjil-{version,20[0-9-]+}/vidjil-algo"
    conda:
        "envs/vidjil.yaml"
    shell:
        install_vidjil(SOFTS_DIR+"/vidjil-{wildcards.version}/", "release-{version}", False, True)

# rule install_vidjil_old:
#     output:
#         SOFTS_DIR+"/vidjil-"+VIDJIL_OLD+"/vidjil-algo"
#     conda:
#         "envs/vidjil.yaml"
#     shell:
#         install_vidjil(SOFTS_DIR+"/vidjil-"+VIDJIL_OLD+"/", "release-2018.02", False, True)

rule install_vidjil_dev:
    output:
        VIDJIL_DIR+"/germline/generate-recombinations.py",
        VIDJIL_DIR+"/germline/homo-sapiens/IGHV.fa",
        VIDJIL_DIR+"/algo/tests/should-vdj-to-tap.py",
        VIDJIL_DIR+"/algo/tests/repseq_vdj.py"
    conda:
        "envs/vidjil.yaml"
    shell:
        install_vidjil(VIDJIL_DIR+"/", "dev", False, False, False)+''';
        echo 59 > {VIDJIL_DIR}/germline/germline_id && make -C {VIDJIL_DIR} germline
        '''
        

rule install_mixcr_library:
    output:
        MIXCR3_LIBRARY_PATH
    shell:
        '''
        wget -O {output} -q {MIXCR3_LIBRARY_url}
        '''

# Doesn't work: weird error "No records with id: TRDV8*01"
rule install_mixcr_custom_library:
    input:
        VIDJIL_DIR+"/germline/homo-sapiens/IGHV.fa"        
    output:
        MIXCR_IMGT_LIBRARY
    conda:
        "envs/mixcr4.yml"
    log:
        LOGS_DIR+"/mixcr-custom-library.log"
    shell:
        '''
        cd {VIDJIL_DIR}/germline/homo-sapiens/
        mkdir -p mixcr
        cp ???V.fa ???D.fa ???J+down.fa mixcr
        cd mixcr
        sed -ri 's/\.//g;/^#/d;s/^>[^|]*\|([^|]*)\|.*$/>\\1/;/^\s*$/d' *.fa
        sed -ri 's/^(>TRAV[^\/]+)\/DV/\\1/' TRAV.fa
        sed -ri 's/^>TRAV[^\/]+\/DV/>TRDV/' TRDV.fa
        (for germline in IGH TRB TRD; do
          MI_LICENSE={MIXCR_LICENSE} mixcr buildLibrary -f --do-not-infer-points --debug --v-genes-from-fasta ${{germline}}V.fa --v-gene-feature VRegion --j-genes-from-fasta ${{germline}}J+down.fa --d-genes-from-fasta ${{germline}}D.fa --chain $germline --species humain hs-$germline.json.gz
        done
        for germline in IGK IGL TRA TRG; do
          MI_LICENSE={MIXCR_LICENSE} mixcr buildLibrary -f --do-not-infer-points --debug --v-genes-from-fasta ${{germline}}V.fa --v-gene-feature VRegion --j-genes-from-fasta ${{germline}}J+down.fa --chain $germline --species humain hs-$germline.json.gz
        done
        MI_LICENSE={MIXCR_LICENSE} mixcr mergeLibrary hs-IGH.json.gz hs-IGK.json.gz hs-IGL.json.gz hs-TRA.json.gz hs-TRB.json.gz hs-TRD.json.gz hs-TRG.json.gz {output}) > {log} 2> {log}
        cd ..; 
        rm -rf mixcr
        '''

        
rule install_igrec:
    output:
        SOFTS_DIR+"/igrec/vj_finder"
    run:
        dir=os.path.dirname(str(output))
        cmd = ["cd {dir}"]

        if not os.path.exists(dir) or not os.path.exists(dir+"/.git"):
            cmd.append("git init")
            cmd.append("git remote add origin https://github.com/ablab/y-tools.git")
        else:
            cmd.append("git checkout -- . && git clean -f");
        cmd.append("git pull origin testing")
        cmd.append("make vjf")
        cmd.append("mv build/release/bin/vj_finder .");
        cmdline = ' && '.join(cmd)
        shell(cmdline)

        
#
# GENERATING DATASET
#

rule random_vdj:
    input:
        VIDJIL_DIR+"/germline/generate-recombinations.py"
    output:
        data = expand(BENCHS_DIR+"/random_vdj-{{error,[0-9.]+}}-{{indel,[0-9.]+}}-{locus}.should-vdj.fa", locus = LOCUS)
    conda:
        "envs/py2.yml"
    shell:
        '''
        cd {VIDJIL_DIR}/germline;
        mkdir -p {VIDJIL_DIR}/data/gen
        python generate-recombinations.py --basename 'random_vdj-'{wildcards.error}-{wildcards.indel} --random-deletions 5,5 --random-insertions 5,5 -n 10 -e {wildcards.error} --error-indel {wildcards.indel} > /dev/null;
        sed -ri '/^\s*$/d' ../data/gen/random_vdj-{wildcards.error}-{wildcards.indel}-*.should-vdj.fa
        mv ../data/gen/random_vdj-{wildcards.error}-{wildcards.indel}-*.should-vdj.fa $(dirname {output[1]});
        '''

rule random_sequences:
    input:
        VIDJIL_DIR+"/germline/homo-sapiens/IGHV.fa"
    output:
        BENCHS_DIR+"/random.fa"
    run:
      import random
      germline_dir=VIDJIL_DIR+"/germline/homo-sapiens/"

      def read_germline(germline):
          f = open(germline, 'r')
          seqs = []
          seq=[]
          for line in f.readlines():
              if line.startswith('>'):
                  if len(seq) > 0:
                      seqs.append(''.join(seq))
                      seq = []
              elif not line.startswith('#'):
                  seq.append(line.rstrip().replace('.', '').lower())
          seqs.append(''.join(seq))
          return seqs

      def shuffle_seq(s):
          l = list(s)
          random.shuffle(l)
          return ''.join(l)

      ighv = read_germline(germline_dir+"/IGHV.fa")
      ighj = read_germline(germline_dir+"/IGHJ+down.fa")

      random_file = open(str(output), 'w')
      for i in range(1000000):
          random_file.write(">seq{}\n".format(i))
          random_file.write(shuffle_seq(random.choice(ighv))+"\n")
          random_file.write(shuffle_seq(random.choice(ighj))+"\n")
        
rule afzal_dataset:
    output:
        expand(BENCHS_DIR+"/afzal-{file}", file=AFZAL_DATASET)
    run:
        import urllib.request
        import os

        for file in list(output):
            afzal_file = os.path.basename(file)[6:]
            urllib.request.urlretrieve('https://www.vidjil.org/data/files/afzal/'+afzal_file, file)

rule ligm_dataset:
    output:
        should_vdj=BENCHS_DIR+"/ligm-db-homo-sapiens.should-vdj.fa",
        should_vidjil=BENCHS_DIR+"/ligm-db-homo-sapiens_for-vidjil.should-vdj.fa"
    shell:
        '''
        wget -O - -q https://www.vidjil.org/data/files/ligm-db-homo-sapiens.should-vdj.fa | awk  '$0~/^>/{{while ($0 !~ /^>.*\[IG[HK]\]/) {{ do {{ getline;}} while ($0 !~ /^>/)}} }} {{print}}' | awk -v RS='>' -v FS='\\n' -v OFS="" '{{id=$1;$1=""; seq=$2; $2=""; comment="\\n"; for (i=3; i< NF; i++) {{ comment=comment$i"\\n";}}; if (! ids[seq]) {{ ids[seq]=id; comments[seq]=comment}} }} END {{ for (seq in ids) {{ if (length(seq)) {{printf ">"ids[seq]"\\n"seq""comments[seq]}}}}}}' > {output.should_vidjil}
        grep -v '^#' {output.should_vidjil} | sed -r '/^[^>]/ s/[^ACGT]/N/g' > {output.should_vdj}
        '''

rule should_vdj_dataset:
    input:
        VIDJIL_DIR+"/algo/tests/should-vdj-to-tap.py"
    output:
        should_vdj=BENCHS_DIR+"/vidjil-designated.should-vdj.fa",
        should_vidjil=BENCHS_DIR+"/vidjil-designated_for-vidjil.should-vdj.fa"
    shell:
        '''
        awk '{{print}}' {VIDJIL_DIR}/algo/tests/should-vdj-tests/*.should-vdj.fa | tee {output.should_vidjil} | grep -v '^#' | tr 'acgt' 'ACGT' | sed -r '/^[^>]/ s/[^ACGT]/N/g' > {output.should_vdj}
        '''
#
# Running software
#

def get_data_file_for_vidjil(wildcards):
    if wildcards.file.endswith('.should-vdj.fa') and not 'random' in wildcards.file:
        return BENCHS_DIR+'/'+wildcards.file[:-len('.should-vdj.fa')]+'_for-vidjil.should-vdj.fa'
    else:
        return BENCHS_DIR+'/{file}'

rule run_vidjil:
    input:
        algo=SOFTS_DIR+'/vidjil-{version}/vidjil-algo',
        sample = get_data_file_for_vidjil
    output:
        results=RESULTS_DIR+'/vidjil-{version}/{file}.results',
        fa=RESULTS_DIR+'/vidjil-{version}/{file}_detect.fa'
    conda:
        "envs/vidjil.yaml"
    log:
        LOGS_DIR+"/vidjil-{version}-{file}.log"
    shell:
        '''
        mkdir -p {RESULTS_DIR}/vidjil-{wildcards.version}
        params=-2
        if [ "{wildcards.version}" == "{VIDJIL_OLD}" ]; then
          params='-# "#"'
        else
          params=$params' --header-sep "#"'
        fi
        out_dir=$(mktemp -d)
        $(which time) --quiet -f "%U\t%M" -o {output.results}.bench {input.algo} -g {SOFTS_DIR}/vidjil-{wildcards.version}/germline $params -c windows -uuuU -o $out_dir  {input.sample} > {log} 2> {log}
        file=$(basename {input.sample})
        cat $out_dir/${{file%.*}}.*ted.vdj.fa > {output.fa}

        file=$out_dir/${{file%.*}}.vidjil

        LOCUS=$(sed -rn 's/^.*((IG[HKL]|TR[ABDG]|TRA\+D)\+?).should-vdj.fa.*$/\\1/p' <<< "{wildcards.file}")
        if [ -z "$LOCUS" ]; then
            SEGMENTED=$(cat $file | jq .reads.segmented[0])
        else
            SEGMENTED=$(cat $file | jq ".reads.germline.\\"$LOCUS\\"[0]")
        fi
        TOTAL=$(cat $file | jq .reads.total[0])
        echo -e "$SEGMENTED\t$TOTAL" > {output.results}

        rm -rf $out_dir
        '''

rule run_vidjil_align:
    input:
        algo=SOFTS_DIR+'/vidjil-{version,202[4-9].*}/vidjil-algo',
        sample = get_data_file_for_vidjil
    conda:
        "envs/vidjil.yaml"
    output:
        out = RESULTS_DIR+'/vidjil-{version}/{file}.align',
        tsv = RESULTS_DIR+'/vidjil-{version}/{file}.tsv',
        vidjil = RESULTS_DIR+'/vidjil-{version}/{file}_assign.vidjil'
    log:
        LOGS_DIR+"/vidjil-{version}-align-{file}.log"
    shell:
        '''
        mkdir -p {RESULTS_DIR}/vidjil-{wildcards.version}
        params="-2"
        if [ "{wildcards.version}" == "{VIDJIL_OLD}" ]; then
          params=$params' -1'
        fi
        out_dir=$(mktemp -d)
        nb_sequences=$(grep -c '^>' "{input.sample}")
        e_value=$(LC_ALL=C awk -v nb=$nb_sequences 'BEGIN{{ printf "%f", 1/nb }}')
        $(which time) --quiet -f "%U\t%M" -o {output.out}.bench {input.algo} -c designations -e $e_value -3 -d -g {SOFTS_DIR}/vidjil-{wildcards.version}/germline $params -o $out_dir {input.sample}  > {log} 2> {log}
        cp {log} {output.out}
        file=$(basename {input.sample})
        file=$out_dir/${{file%.*}}
        mv $file.tsv {output.tsv}
        mv $file.vidjil {output.vidjil}
        rm -rf $out_dir/
        '''
        
rule run_vidjil_align_old:
    input:
        algo=SOFTS_DIR+'/vidjil'+VIDJIL_OLD+'/vidjil-algo',
        sample = get_data_file_for_vidjil
    output:
        out = RESULTS_DIR+'/vidjil'+VIDJIL_OLD+'/{file}.align',
        vidjil = RESULTS_DIR+'/vidjil'+VIDJIL_OLD+'/{file}_assign.vidjil'
    conda:
        "envs/vidjil.yaml"
    log:
        LOGS_DIR+"/vidjil"+VIDJIL_OLD+"-align-{file}.log"
    shell:
        '''
        mkdir -p {RESULTS_DIR}/vidjil-{VIDJIL_OLD}
        params=-2
        out_dir=$(mktemp -d)
        nb_sequences=$(grep -c '^>' "{input.sample}")
        e_value=$(LC_ALL=C awk -v nb=$nb_sequences 'BEGIN{{ printf "%f", 1/nb }}')
        $(which time) --quiet -f "%U\t%M" -o {output.out}.bench {input.algo} -c segment -e $e_value -3 -d -g {SOFTS_DIR}/vidjil-{VIDJIL_OLD}/germline $params -o $out_dir {input.sample} > {log} 2> {log}
        cp {log} {output.out} 
        file=$(basename {input.sample})
        file=$out_dir/${{file%.*}}
        mv $file.vidjil {output.vidjil}
        rm -rf $out_dir/
        '''
        
rule run_mixcr4:
    input:
        sample = BENCHS_DIR+'/{file}',
        library = MIXCR_IMGT_LIBRARY
    output:
        RESULTS_DIR+'/mixcr4/{file}.results'
    conda:
        "envs/mixcr4.yml"
    log:
        LOGS_DIR+"/mixcr4-{file}.log"
    shell:
        '''
          mkdir -p RESULTS_DIR/mixcr4
          MI_LICENSE={MIXCR_LICENSE} $(which time) --quiet -f "%U\t%M" -o {output}.bench mixcr align --preset generic-amplicon --dna --floating-left-alignment-boundary --floating-right-alignment-boundary J --library {MIXCR_LIB_BASENAME} -s humain -t 1 -f {input.sample} {output}.vdjca > {log} 2> {log}
          LOCUS=$(sed -rn 's/^.*((IG[HKL]|TR[ABDG]|TRA\+D)\+?).should-vdj.fa.*$/\\1/p' <<< "{wildcards.file}")
          awk -v locus="$LOCUS" 'BEGIN{{locusre = "^"locus" chains"}} $0 ~ /^Total sequencing/ {{total=$NF}} $0 ~ /^Successfully aligned/ {{aligned=$(NF-1)}} length(locus) > 0 && $0 ~ locusre {{ aligned=$3 }} END {{printf aligned"\\t"total"\\n"}}' {log} > {output}
        '''

rule run_mixcr4_align:
    input:
        sample = BENCHS_DIR+'/{file}',
        library = MIXCR_IMGT_LIBRARY
    output:
        tsv=RESULTS_DIR+'/mixcr4/{file}.tsv',
        vdjca=RESULTS_DIR+"/mixcr4/{file}.vdjca"
    conda:
        "envs/mixcr4.yml"
    log:
        LOGS_DIR+"/mixcr4-align-{file}.log"
    shell:
        '''
          mkdir -p {RESULTS_DIR}/mixcr4
          MI_LICENSE={MIXCR_LICENSE} $(which time) --quiet -f "%U\t%M" -o {output.tsv}.bench mixcr align --preset generic-amplicon --dna --floating-left-alignment-boundary --floating-right-alignment-boundary J --library {MIXCR_LIB_BASENAME} -s humain -OsaveOriginalReads=true -t 1 -f {input.sample} {output.vdjca} > {log} 2> {log}
          MI_LICENSE={MIXCR_LICENSE} mixcr exportAlignments -descrsR1 -vHit -dHit -jHit -nFeature VDJunction -nFeature DJJunction -nFeature VJJunction -aaFeature CDR3 {output.vdjca} > "{output.tsv}"
        '''

rule run_mixcr3:
    input:
        sample = BENCHS_DIR+'/{file}',
        library = MIXCR3_LIBRARY_PATH
    output:
        RESULTS_DIR+'/mixcr3/{file}.results'
    conda:
        "envs/mixcr3.yml"
    log:
        LOGS_DIR+"/mixcr3-{file}.log"
    shell:
        '''
          mkdir -p {RESULTS_DIR}/mixcr3
          cd {SOFTS_DIR}
          MI_LICENSE={MIXCR_LICENSE} $(which time) --quiet -f "%U\t%M" -o {output}.bench mixcr align --library imgt -s hsa -t 1 -f {input.sample} {output}.vdjca > {log} 2> {log}
          LOCUS=$(sed -rn 's/^.*((IG[HKL]|TR[ABDG]|TRA\+D)\+?).should-vdj.fa.*$/\\1/p' <<< "{wildcards.file}")
          awk -v locus="$LOCUS" 'BEGIN{{locusre = "^"locus" chains"}} $0 ~ /^Total sequencing/ {{total=$NF}} $0 ~ /^Successfully aligned/ {{aligned=$(NF-1)}} length(locus) > 0 && $0 ~ locusre {{ aligned=$3 }} END {{printf aligned"\\t"total"\\n"}}' {log} > {output}
        '''

rule run_mixcr3_align:
    input:
        sample = BENCHS_DIR+'/{file}',
        library = MIXCR3_LIBRARY_PATH
    output:
        tsv=RESULTS_DIR+'/mixcr3/{file}.tsv',
        vdjca=RESULTS_DIR+"/mixcr3/{file}.vdjca"
    conda:
        "envs/mixcr3.yml"
    log:
        LOGS_DIR+"/mixcr3-align-{file}.log"
    shell:
        '''
          mkdir -p {RESULTS_DIR}/mixcr3
          cd {SOFTS_DIR}
          MI_LICENSE={MIXCR_LICENSE} $(which time) --quiet -f "%U\t%M" -o {output.tsv}.bench mixcr align --library imgt -s hsa -OsaveOriginalReads=true -t 1 -f {input.sample} {output.vdjca} > {log} 2> {log}
          MI_LICENSE={MIXCR_LICENSE} mixcr exportAlignments -descrsR1 -vHit -dHit -jHit -nFeature VDJunction -nFeature DJJunction -nFeature VJJunction -aaFeature CDR3 {output.vdjca} > "{output.tsv}"
        '''
        
rule run_igrec:
    input:
        algo=SOFTS_DIR+'/igrec/vj_finder',
        sample = BENCHS_DIR+'/{file}',
    output:
        RESULTS_DIR+'/igrec/{file}.results'
    shell:
        '''
        OUT_DIR=$(mktemp -d)
        cd $(dirname {input.algo});
        $(which time) --quiet -f "%U\t%M" -o {output}.bench {input.algo} --loci all --organism human --pseudogenes=on -t 1 --db-directory data/germline -i {input.sample} -o $OUT_DIR 2>&1 | tee {output}.err | grep -Po '[0-9]+ reads were aligned; [0-9]+ reads were filtered out' | awk '{{printf "%d\\t%d\\n",$1,$1+$5}}' > {output} 
        rm -rf $OUT_DIR
        '''
        
rule run_igrec_align:
    input:
        algo=SOFTS_DIR+'/igrec/vj_finder',
        sample = BENCHS_DIR+'/{file}',
    output:
        RESULTS_DIR+'/igrec/{file}.csv'
    shell:
        '''
        OUT_DIR=$(mktemp -d)
        cd $(dirname {input.algo});
        $(which time) --quiet -f "%U\t%M" -o {output}.bench {input.algo} --loci all --organism human --pseudogenes=on -t 1 --db-directory data/germline -i {input.sample} -o $OUT_DIR  2> /dev/null &>1
        mv $OUT_DIR/alignment_info.csv {output}
        rm -rf $OUT_DIR
        '''

#
# Make VDJ files
#

rule mixcr_to_vdj:
    input:
        tsv=RESULTS_DIR+'/mixcr{version}/{file}.tsv',
        script=VIDJIL_DIR+'/algo/tests/repseq_vdj.py'
    output:
        RESULTS_DIR+'/mixcr{version}/{file}.vdj'
    conda:
        "envs/py2.yml"
    log:
        LOGS_DIR+"mixcr{version}-to-vdj-{file}.log"
    shell:
        '''
        python {input.script} {input.tsv} > {output} 2> {log}
        '''

rule igrec_to_vdj:
    input:
        csv=RESULTS_DIR+'/igrec/{file}.csv',
        script=VIDJIL_DIR+'/algo/tests/repseq_vdj.py'
    output:
        RESULTS_DIR+'/igrec/{file}.vdj'
    conda:
        "envs/py2.yml"
    shell:
        '''
        python {input.script} {input.csv} > {output} 2> {output}.err
        '''

rule vidjil_detect_to_vdj:
    input:
        fa=RESULTS_DIR+"/vidjil-{version}/{file}_detect.fa",
        script=VIDJIL_DIR+'/algo/tests/repseq_vdj.py'
    output:
        RESULTS_DIR+"/vidjil-{version}/{file}_detect.vdj"
    conda:
        "envs/py2.yml"
    log:
        LOGS_DIR+"/vidjil-{version}-to-vdj-{file}.log"
    shell:
        '''
        python {input.script} {input.fa} > {output} 2> {log}
        '''
        
rule vidjil_to_vdj:
    input:
        vdj=RESULTS_DIR+"/vidjil-{version}/{file}_assign.vidjil",
        fasta=BENCHS_DIR+"/{file}",
        script=VIDJIL_DIR+'/algo/tests/repseq_vdj.py'
    output:
        RESULTS_DIR+"/vidjil-{version}/{file}_assign.vdj"
    conda:
        "envs/py2.yml"
    log:
        LOGS_DIR+"/vidjil-{version}-assign-to-vdj-{file}.log"
    shell:
        '''
        python {input.script} {input.fasta} {input.vdj} > {output} 2> {log}
        '''

#
# Assess assign / detection
#

def files_for_assignment_detection(wildcards):
    if not wildcards.softs.startswith('vidjil'):
        return [ RESULTS_DIR+'/'+wildcards.softs+'/{file}.vdj',
                 RESULTS_DIR+'/'+wildcards.softs+'/{file}.vdj']
    else:
        return [RESULTS_DIR+'/'+wildcards.softs+'/{file}_detect.vdj',
                RESULTS_DIR+'/'+wildcards.softs+'/{file}_assign.vdj']

rule vdj_to_assign_detection_results:
    input:
        files_for_assignment_detection,
        VIDJIL_DIR+"/algo/tests/should-vdj-to-tap.py"
    output:
        detect=RESULTS_DIR+'/{softs}/{file}.vdj.detect',
        assign=RESULTS_DIR+'/{softs}/{file}.vdj.assign',
    conda:
        "envs/py2.yml"
    shell:
        '''
        filter_should_vdj_results() {{
          awk '$1 == "===" {{ok=1}} ok == 1 && length($1) >= 3 && length($1) <= 5 && $1 != "None" && $3 ~ /^[0-9]+$/ {{print $1"\t"$3}}' | LANG=C sort
        }}

        echo -e "locus\t{wildcards.softs}" > {output.detect}
        echo -e "locus\t{wildcards.softs}" > {output.assign}

        (python {VIDJIL_DIR}/algo/tests/should-vdj-to-tap.py -2 {input[0]} || true) | tee {output.detect}.raw | filter_should_vdj_results >> {output.detect}
        (python {VIDJIL_DIR}/algo/tests/should-vdj-to-tap.py --ignore_del --ignore_D --ignore_allele --ignore_N -3 {input[1]} || true) | tee {output.assign}.raw | filter_should_vdj_results >> {output.assign}
        '''

#
# Gather results
#


def gather_results(filenames, output):
    out = open(str(output), 'w')
    base_files = list(set([os.path.splitext(os.path.basename(f))[0] for f in filenames]))
    
    for filename in filenames:
        dirs = filename.split('/')
        soft = dirs[-2]
        locus = None
        if filename.endswith('.should-vdj.fa.results'):
            locus = os.path.basename(filename[:-len('.should-vdj.fa.results')]).split('-')[-1]
            if locus not in LOCUS:
                locus = None


        bench = open(filename+".bench").readlines()[0].rstrip()
        results = open(filename).readlines()[0].rstrip()
        if locus:
            print(locus, soft, bench, results, sep='\t', file=out)
        else:
            if len(base_files)>1:
                raise ValueError('Several different names: '+', '.join(list(base_files)))
            print(base_files[0], soft, bench, results, sep='\t', file=out)
    out.close()

rule gather_random_vdj_results:
    input:
        complete=expand(RESULTS_DIR+"/{softs}/random_vdj-{{error}}-{locus}.should-vdj.fa.results", softs=SOFTS, locus=COMPLETE_LOCUS),
        incomplete=expand(RESULTS_DIR+"/{softs}/random_vdj-{{error}}-{locus}.should-vdj.fa.results", softs=SOFTS, locus=INCOMPLETE_LOCUS)
    output:
        all=RESULTS_DIR+"/final/random_vdj-{error}.tsv",
        complete=RESULTS_DIR+"/final/random_vdj-{error}_complete.tsv",
        incomplete=RESULTS_DIR+"/final/random_vdj-{error}_incomplete.tsv"
    run:
        gather_results(input.complete, output.complete)
        gather_results(input.incomplete, output.incomplete)
        gather_results(input.complete+input.incomplete, output.all)

rule gather_classic_results:
    input:
        expand(RESULTS_DIR+'/{softs}/{{file}}.results', softs = SOFTS)
    output:
        RESULTS_DIR+"/final/{file}.tsv"
    run:
        gather_results(input, output)

rule gather_should_vdj_results:
    input:
        tsv=expand(RESULTS_DIR+"/{softs}/{{file}}.should-vdj.fa.vdj.{{mode}}",
               softs = SOFTS),
        sequences=BENCHS_DIR+"/{file}.should-vdj.fa"
    output:
        RESULTS_DIR+"/final/bench_{file}_shouldvdj.{mode}.tsv"
    run:
        import pandas

        def sort_locus(locus):
            if '+' in locus:
                return chr(9999)+locus
            return locus
        

        merged_file = pandas.read_csv(input.tsv[0], delimiter='\t')
        nb = {}
 
        for csv in input.tsv[1:]:
            data = pandas.read_csv(csv, delimiter='\t')
            data = data.drop(data[data.iloc[:,1]==0].index) # drop 0 values
            merged_file = pandas.merge(merged_file, data, on='locus', how='outer')
        merged_file = merged_file.astype({col: 'Int64' for col in merged_file.columns[1:]})
        all_locus = [ x for x in merged_file['locus'] if isinstance(x, str) ]
        for locus in all_locus:
            nb[locus] = sum([ re.match(r'^>.*\['+locus.replace('+', '\+')+'\].*$', l.rstrip()) is not None for l in open(input.sequences).readlines()])
        nb_seq = pandas.DataFrame.from_dict(nb, orient='index', columns=['Nb sequences'])
        nb_seq.insert(0, "locus", nb_seq.index)
        nb_seq = nb_seq.reset_index(drop=True)
        merged_file = pandas.merge(nb_seq, merged_file, on='locus', how='outer')
        all_locus.sort(key = sort_locus)
        merged_file.sort_values('locus', inplace=True, key=lambda x : x.map(lambda e: all_locus.index(e)))
        merged_file.to_csv(str(output), sep='\t', index=False)

rule afzal_summary_per_type:
    input:
        expand(RESULTS_DIR+"/final/afzal-file_{{error}}error_{type}sample.1.fastq.gz.tsv", type=['B', 'C', 'D'])
    output:
        RESULTS_DIR+"/final/afzal-file_{error}error.tsv"
    conda:
        "envs/py3.yml"
    shell:
        '''
        err="Low error"
        if [ "{wildcards.error}" == "0.01-0.0002" ]; then
          err="Medium error"
        fi
        cat {input} | mlr --tsv --hi --ho stats1 -a mean -f 3,4,5,6 -g 2 then put '$1="'"$err"'"' then reorder -f 1 > {output}
        '''
    
rule afzal_summary:
    input:
        medium=RESULTS_DIR+"/final/afzal-file_0.01-0.0002error.tsv",
        low=RESULTS_DIR+"/final/afzal-file_0.001-0.0002error.tsv"
    output:
        RESULTS_DIR+"/final/afzal_summary.tsv"
    shell:
        '''
        cat {input.low} {input.medium} > {output}
        '''
#
# Generate plots
#

rule results_to_plot:
    input:
        RESULTS_DIR+"/final/{file}.tsv"
    output:
        RESULTS_DIR+"/final/{file}.pdf"
    conda:
        "envs/py3.yml"
    shell:
        '''
        python3 create_plot.py {input}
        '''

rule shouldvdj_export_results:
    input:
        assign=RESULTS_DIR+"/final/bench_{file}_shouldvdj.assign.tsv",
        detect=RESULTS_DIR+"/final/bench_{file}_shouldvdj.detect.tsv",
        sequences=BENCHS_DIR+"/{file}.should-vdj.fa"
    output:
        RESULTS_DIR+"/final/bench_{file}_shouldvdj.pdf",
        RESULTS_DIR+"/final/bench_{file}_shouldvdj.tex"
    conda:
        'envs/py3.yml'
    shell:
        '''
        python3 create_should_plot.py {input.detect} {input.assign} {input.sequences}
        '''
